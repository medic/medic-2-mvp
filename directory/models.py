# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from django.utils.encoding import python_2_unicode_compatible

from cloudinary.models import CloudinaryField
from django.urls import reverse

# Specialties
class Specialty(models.Model):    
    name = models.CharField('especialidad', max_length=100)
    denomination = models.CharField('denominación',max_length=100)
    
    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Especialidad'
        verbose_name_plural = 'Especialidades'

class Region(models.Model):
    name = models.CharField('Región', max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Región'
        verbose_name_plural = 'Regiones'



class City(models.Model):
    name = models.CharField('Ciudad', max_length=255)
    region = models.ForeignKey(Region, null=True)
    
    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name = 'Ciudad'
        verbose_name_plural = 'Ciudades'


# Doctor information
class Doctor(models.Model):    
    active = models.BooleanField('activo', default=True)
    firstname = models.CharField('nombre', max_length=50)
    lastname = models.CharField('apellido', max_length=50)
    mpps = models.CharField('MPPS', max_length=20, unique=True)
    registration_number = models.CharField('Numero de registro adicional', max_length=20, unique=True)
    denomination = models.CharField('Denominación', max_length=5, choices=(('Dr.', 'Dr.'), ('Dra.', 'Dra.')))        
    specialty = models.ManyToManyField(Specialty, verbose_name='Especialidad')

    def __str__(self):        
        return '%s %s %s' % (self.denomination, self.firstname, self.lastname)

    def display_fullname(self):        
        return '%s %s' % (self.firstname, self.lastname)

    def display_specialties(self):
       specialties = ' - '.join([p.denomination for p in self.specialty.all()])
       aditional_specialty = self.profile.additional_specialty
       return specialties if not aditional_specialty else '%s - %s' % (specialties, aditional_specialty)

    def display_contacts(self):
        return ' / '.join([contact.phone for contact in self.contact.all()])

    def get_absolute_url(self):
        return reverse('directory:profile', kwargs={'display_name': self.profile.display_name})

    class Meta:
        verbose_name = 'Doctor'
        verbose_name_plural = 'Doctores'


# Doctor's contact numbers
class DoctorContact(models.Model):
    doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE, related_name='contact')
    phone = models.CharField('teléfono', max_length=16)

    def __str__(self):
        return self.phone

    class Meta:
        verbose_name = 'Número de contacto'
        verbose_name_plural = 'Números de contacto'


# Doctor's social contact
class DoctorSocial(models.Model):
    doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE, related_name='social')
    social_type = models.CharField('red social', max_length=20, choices=(('facebook', 'facebook'), ('twitter', 'twitter'), ('mail', 'mail'), ('instagram', 'instagram')))
    social_id = models.CharField('usuario', max_length=50)

    def get_url(self):
        if self.social_type == "facebook":
            return "https://facebook.com/%s" % (self.social_id)

        if self.social_type == "twitter":
            return "https://twitter.com/%s" % (self.social_id)

        if self.social_type == "instagram":
            return "https://instagram.com/%s" % (self.social_id)

        if self.social_type == "mail":
            return "mailto:%s" % (self.social_id)

        return None

    class Meta:
        verbose_name = 'Red social'
        verbose_name_plural = 'Redes sociales'


# Doctor's services
class DoctorService(models.Model):
    doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE, related_name='service')
    name = models.CharField('servicio', max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Servicio'
        verbose_name_plural = 'Servicios'

class DoctorSchedule(models.Model):
    doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE, related_name='schedule_attention')    
    day = models.CharField('días', max_length=50)
    workday = models.CharField('horario', max_length=50)

    def __str__(self):
        return '%s %s' % (self.day, self.workday)

    class Meta:
        verbose_name = 'Horario de atención'
        verbose_name_plural = 'Horarios de atención'

# Attention Center
class AttentionCenter(models.Model):
    name = models.CharField('lugar', max_length=255)
    address = models.CharField('dirección', max_length=255)
    longitude = models.CharField('longitud', max_length=50)
    latitude = models.CharField('latitud', max_length=50)
    city = models.ForeignKey(City, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Centro de atención'
        verbose_name_plural = 'Centros de atención'


# Doctor's office
class DoctorOffice(models.Model):
    doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE, verbose_name='Doctor', related_name='office')
    attention_center = models.ForeignKey(AttentionCenter, on_delete=models.CASCADE, verbose_name='Centro Médico', related_name='office_attentioncenter')
    description = models.CharField('consultorio', max_length=255)    

    def __str__(self):
        return '%s. %s - %s' % (self.attention_center.name, self.attention_center.address, self.description)

    class Meta:
        verbose_name = 'Consultorio'
        verbose_name_plural = 'Consultorios'


# Profile related information (template)
class Profile(models.Model):        
    doctor = models.OneToOneField(Doctor, on_delete=models.CASCADE, primary_key=True)
    display_name = models.CharField('usuario medic', max_length=50, unique=True)
    description = models.CharField('descripción', max_length=255)
    additional_specialty = models.CharField('especialidad adicional', null=True, blank=True, max_length=255)
    city = models.ForeignKey(City, on_delete=models.CASCADE, verbose_name='Ciudad')
    #photo = models.ImageField(upload_to = 'profiles', default='profile/user-pic.png', verbose_name='Foto de perfil')
    photo = CloudinaryField("Foto de perfil", blank=True, null=True)

    class Meta:
        verbose_name = 'Perfil'
        verbose_name_plural = 'Perfil'


# Account related information (sms, email notifications)
class Account(models.Model):
    doctor = models.OneToOneField(Doctor, on_delete=models.CASCADE, primary_key=True)
    email = models.EmailField('correo electrónico', max_length=100, help_text='Correo al que llegaran notificaciones.')
    phone = models.CharField('teléfono', max_length=12, help_text='Celular al que llegaran notificaciones de mensaje de texto.')

    class Meta:
        verbose_name = 'Cuenta'
        verbose_name_plural = 'Cuentas'