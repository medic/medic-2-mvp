# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Account, Doctor, Profile, Specialty, DoctorContact, DoctorSocial, DoctorService, DoctorSchedule, Profile, City, AttentionCenter, DoctorOffice, Region
from payments.models import DoctorPaymentPreferences

from reservations.models import ScheduleBlock

@admin.register(Specialty)
class SpecialtyAdmin(admin.ModelAdmin):
    list_display = ('name', 'denomination')

class DoctorContactInLine(admin.TabularInline):
    model = DoctorContact

class DoctorSocialInLine(admin.TabularInline):
    model = DoctorSocial

class DoctorServiceInLine(admin.TabularInline):
    model = DoctorService

class DoctorScheduleInLine(admin.TabularInline):
    model = DoctorSchedule

class ProfileInLine(admin.StackedInline):
    model = Profile

class AccountInLine(admin.StackedInline):
    model = Account

class ScheduleBlockInline(admin.TabularInline):
    model = ScheduleBlock

class DoctorOfficeInline(admin.TabularInline):
    model = DoctorOffice

class DoctorPaymentPreferencesInline(admin.TabularInline):
    model = DoctorPaymentPreferences

@admin.register(Doctor)
class DoctorAdmin(admin.ModelAdmin):
    list_display = ('firstname', 'lastname', 'mpps', 'display_specialties')
    inlines = [
        DoctorServiceInLine,
        DoctorOfficeInline,
        DoctorContactInLine,
        DoctorScheduleInLine,
        ScheduleBlockInline,
        DoctorSocialInLine,
        ProfileInLine,
        AccountInLine,
        DoctorPaymentPreferencesInline,
    ]

@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    list_display = ('name',)

@admin.register(Region)
class RegionAdmin(admin.ModelAdmin):
    list_display = ('name',)

@admin.register(AttentionCenter)
class AttentionCenterAdmin(admin.ModelAdmin):
    list_display = ('name', 'address',)