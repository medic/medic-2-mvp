# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, get_object_or_404

from .models import Specialty, City, Doctor, Profile



def landing_directory(request):
    specialties = Specialty.objects.all()
    cities = City.objects.all()
    doctors = Doctor.objects.filter(active=True)

    params = {
        'specialties': specialties,
        'cities': cities,
        'doctors': doctors
    }
    return render(request, 'landing_directory.html', params)

def profile(request, display_name):
    doctor = get_object_or_404(Profile, display_name=display_name).doctor
    params = {}

    if doctor:        
        params = {
            'doctor': doctor,
            'doctor_specialties': doctor.display_specialties,
            'doctor_office': doctor.office.first(),
            'price': doctor.payment_preferences
        }

    return render(request, 'profile.html', params)