from django.conf.urls import url
from django.contrib.sitemaps.views import sitemap
from django.views.generic import TemplateView
from .sitemaps import make_sitemap
from . import views

urlpatterns = [
    url(r'^$', views.landing_directory, name='landing_directory'),
    url(r'^(?P<display_name>\w+)/$', views.profile, name='profile'),
    url(r'^sitemap\.xml$', sitemap, {'sitemaps': make_sitemap()}, name='django.contrib.sitemaps.views.sitemap'),
    url(r'^robots\.txt', TemplateView.as_view(template_name='robots.txt', content_type='text/plain'), name='robots_file'),
]