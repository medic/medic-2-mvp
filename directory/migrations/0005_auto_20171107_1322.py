# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-11-07 13:22
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('directory', '0004_auto_20171105_2101'),
    ]

    operations = [
        migrations.AlterField(
            model_name='doctor',
            name='cm',
            field=models.CharField(max_length=20, unique=True, verbose_name='Numero de registro adicional'),
        ),
        migrations.AlterField(
            model_name='doctorsocial',
            name='social_type',
            field=models.CharField(choices=[('facebook', 'facebook'), ('twitter', 'twitter'), ('mail', 'mail'), ('instagram', 'instagram')], max_length=20, verbose_name='red social'),
        ),
    ]
