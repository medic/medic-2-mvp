from django.contrib.sitemaps import Sitemap
from django.urls import reverse
from .models import Doctor

class StaticViewSitemap(Sitemap):
    priority = 1
    changefreq = 'monthly'

    def items(self):
        return ['directory:landing_directory']

    def location(self, item):
        return reverse(item)

class DoctorSitemap(Sitemap):
    priority = 1
    changefreq = 'monthly'
    
    def items(self):
        return Doctor.objects.filter(active=True)

def make_sitemap():
    sitemaps = {
        'static':StaticViewSitemap,
        'blog': DoctorSitemap
    }

    return sitemaps