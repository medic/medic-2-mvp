from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^api/calendar/(?P<display_name>\w+)/$', views.endpoint_calendar_handler, name='calendar'),
    url(r'^(?P<display_name>\w+)/$', views.reservation_handler, name='reservation'),
]