from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from utils.utils import to_date_format, to_24h_format, to_phone_format
from utils.reservation_utils import get_week
from .models import Reservation, ScheduleBlock
from directory.models import Profile

import json

@csrf_exempt
def endpoint_calendar_handler(request, display_name):
    if request.method == 'POST' and request.is_ajax():
        start_date = to_date_format(request.POST.get('start_date'))
        today_date = to_date_format(request.POST.get('today_date'))
        on_load = int(request.POST.get('on_load'))
        
        calendar_week = get_week(on_load, display_name, start_date, today_date)

        json_calendar = {
            'working_days': calendar_week['times_block'],
            'start_week': calendar_week['start_week'],
            'max_advance': 1 #TODO colocarlo desde modelo
        }

        return HttpResponse(json.dumps(json_calendar), content_type="application/json")

def reservation_handler(request, display_name):
    
    if request.method == 'POST':
        r_date = to_date_format(request.POST.get('reservation-date'))
        r_day = r_date.strftime('%A').lower()
        r_start_time = to_24h_format(request.POST.get('block-time-start'))
        r_end_time = to_24h_format(request.POST.get('block-time-end'))
        r_block_number = request.POST.get('block-number')
        r_block_limit = request.POST.get('block-limit')

        r_client_id = request.POST.get('nationality') + request.POST.get('identification')

        r_client_phone = to_phone_format(request.POST.get('phone'))

        r_client_phone = r_client_phone if r_client_phone[0] == '0' else '0' + r_client_phone

        r_doctor = get_object_or_404(Profile, display_name=display_name).doctor
        r_block = ScheduleBlock.objects.get(pk=r_block_number)

        reservation = Reservation(
            appointment_date = r_date,
            block = r_block,
            doctor = r_doctor,
            client_name = request.POST.get('name'),
            identity_card = request.POST.get('identification'),
            nationality = request.POST.get('nationality'),
            phone = r_client_phone,
            email = request.POST.get('email')
        )

        reservation.save()
        
        return redirect(reverse('payments:mp_create_order', kwargs={'reservation_id': reservation.pk}))