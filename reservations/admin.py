from django.contrib import admin

from .models import Reservation

@admin.register(Reservation)
class ReservationAdmin(admin.ModelAdmin):
    list_display = ('appointment_date', 'block', 'status', 'doctor', 'payment_id', 'client_name', 'identity_card', 'nationality', 'phone', 'email', 'created_at', 'confirmed_at')