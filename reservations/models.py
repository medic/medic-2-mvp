from django.db import models
from django.core.validators import MinValueValidator
from directory.models import Doctor

class ScheduleBlock(models.Model): 
    doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE, related_name='schedule_block')
    day = models.CharField('día', max_length=10, choices=(('monday', 'Lunes'), ('tuesday', 'Martes'), ('wednesday', 'Miércoles'), ('thursday', 'Jueves'), ('friday', 'Viernes')))    
    block = models.PositiveSmallIntegerField('bloque', validators=[MinValueValidator(1)], null=False, blank=False)
    limit = models.PositiveSmallIntegerField('cupos permitidos', validators=[MinValueValidator(1)], null=True, blank=True)
    start = models.TimeField('Hora de inicio')
    end = models.TimeField('Hora de finalización')
    available = models.BooleanField('Disponibilidad', default=True)

    def __str__(self):
        return '%s de %s a %s' % (self.get_day_display(), self.start, self.end)

    def display_block(self):
        return '%s a %s' % (self.start.strftime('%I:%H %p'), self.end.strftime('%I:%H %p'))

    class Meta:
        verbose_name = 'Bloques de consulta'
        verbose_name_plural = 'Bloques de consulta'


class Reservation(models.Model):    
    doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE, related_name='reserved_doctor')
    block = models.ForeignKey(ScheduleBlock, on_delete=models.CASCADE, related_name='reserved_block')
    appointment_date = models.DateField('Fecha de reserva', null=False, blank=False)
    status = models.CharField('Estatus', max_length=20, default='NO PROCESADA', choices=(('NO PROCESADA', 'No procesada'), ('PROCESADA', 'Procesada'), ('CONFIRMADA', 'Confirmada')))
    payment_id = models.CharField('Id de pago', max_length=255)
    client_name = models.CharField('Cliente', max_length=255, null=False, blank=False)
    identity_card = models.CharField('Cédula', max_length=12, null=False, blank=False)
    nationality = models.CharField('Nacionalidad', max_length=1, choices=(('V', 'V'), ('E', 'E')))
    phone = models.CharField('teléfono', max_length=20)
    email = models.EmailField('correo electrónico', max_length=100)
    created_at = models.DateTimeField('Creado', auto_now_add=True, null=True)
    confirmed_at = models.DateTimeField('Confirmado', null=True)

    def display_clientid(self):
        return ('%s%s') % (self.nationality, self.identity_card)

    class Meta:
        verbose_name = 'Reservación'
        verbose_name_plural = 'Reservaciones'
