from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^auth/mp_app/start/(?P<doctor_id>\d+)$', views.auth_mp_app, name='auth_mp_app'),
    url(r'^auth/mp_app/callback/(?P<doctor_id>\d+)$', views.save_mp_credentials, name='auth_mercadopago'),
    url(r'^mercadopago/create/(?P<reservation_id>\d+)$', views.mp_create_order, name='mp_create_order'),
    url(r'^mercadopago/payment/(?P<reservation_id>\d+)$', views.mp_payment_success, name='mp_payment_success'),
    url(r'^mercadopago/notification/$', views.mp_notification, name='mp_notification'),
]  