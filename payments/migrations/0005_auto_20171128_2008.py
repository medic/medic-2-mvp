# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-11-28 20:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0004_mercadopagocredentials_refresh_token'),
    ]

    operations = [
        migrations.AlterField(
            model_name='doctorpaymentpreferences',
            name='mp_method',
            field=models.CharField(choices=[('mp_standard', 'Mercado Pago Standard'), ('mp_marketplace', 'Mercado Pago Marketplace')], default='mp_standard', max_length=20, verbose_name='método de pago'),
        ),
    ]
