from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse

from directory.models import Doctor
from reservations.models import Reservation, ScheduleBlock
from .models import *

import logging
import mercadopago
import os
from urllib.parse import urlencode
from httplib2 import Http
import json

from django.http import HttpResponse

from data.payments_data import MP_CHECKOUT_PREFERENCES
from utils.utils import date_to_string_format
from utils.sms_utils import TextveloperImproved
from utils.mail_utils import send_mail
from data.sms_data import SMS_TEMPLATES
from data.mail_data import MAIL_RESERVATION_TEMPLATES
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime

from utils.logger import log_info

import re
import requests

# Create your views here.

MP_APP_ID = os.environ['MP_APP_ID']
MP_APP_SECRET = os.environ['MP_SECRET_KEY']

SMS_TOKEN_ACCOUNT = os.environ['SMS_TOKEN_ACCOUNT']
SMS_TOKEN_APP = os.environ['SMS_TOKEN_APP']

def _get_auth_redirect_uri(request, doctor_id):
    return 'https://' + request.get_host() + reverse('payments:auth_mercadopago', kwargs={'doctor_id': doctor_id})

def auth_mp_app(request, doctor_id):
    
    doctor = get_object_or_404(Doctor, pk=doctor_id)

    params = {
            'url_auth': _get_auth_redirect_uri(request, doctor_id),
            'client_id': MP_APP_ID
        }

    return render(request, 'mp_auth_app.html', params)

def save_mp_credentials(request, doctor_id):
    
    doctor = get_object_or_404(Doctor, pk=doctor_id)
    
    code = request.GET['code']
    headers = { 'accept': 'application/json', 'content-type': 'application/x-www-form-urlencoded'}
    body = {'client_id': MP_APP_ID,
                'client_secret': MP_APP_SECRET,
                'grant_type': 'authorization_code',
                'code': code,
                'redirect_uri': _get_auth_redirect_uri(request, doctor_id)
                }  

    http = Http()
    response, content = http.request('https://api.mercadopago.com/oauth/token', 'POST', urlencode(body), headers)

    data = json.loads(content)

    params = {
        'primary_text': u'Se ha producido un error',
        'secondary_text': u'ContÃ¡ctenos para revisar que ha podido pasar.',
        'icon_text':'alert'
    }

    try:
        if data:
            access_token = data['access_token']
            public_key = data['public_key']
            refresh_token = data['refresh_token']
            
            if MercadopagoCredentials.objects.filter(pk=doctor_id).exists():
                params = {
                    'primary_text': u'OperaciÃ³n no realizada',
                    'secondary_text': u'Esta cuenta ya se encuentra vinculada, consulte a su administrador.',
                    'icon_text':'alert'
                }               
            else:                
                credentials = MercadopagoCredentials(access_token=access_token, public_key=public_key, refresh_token=refresh_token, doctor=doctor)                
                credentials.save()

                params = {
                    'primary_text': u'Cuenta autorizada con Ã©xito',
                    'secondary_text': u'Ya puede comenzar a recibir pagos en su perfil',
                    'icon_text':'check'
                }
    except KeyError as e:
        print("No se encontrÃ³ refresh_token")

    return render(request, 'mp_auth_success.html', params)
    
def mp_create_order(request, reservation_id):
    
    if request.method == 'GET':
        reservation = Reservation.objects.get(pk=reservation_id)
        doctor = reservation.doctor

        d_payment_preferences = doctor.payment_preferences

        if d_payment_preferences.mp_method == 'mp_marketplace':
            account = MercadopagoCredentials.objects.get(doctor=doctor)
            mp = mercadopago.MP(account.access_token)
        else:
            mp = mercadopago.MP(MP_APP_ID, MP_APP_SECRET)

        mp_checkoutpreferences = dict(MP_CHECKOUT_PREFERENCES)

        preference = {
            'items': [
                {
                    'title': mp_checkoutpreferences['title'],
                    'description': mp_checkoutpreferences['description'],
                    'quantity': mp_checkoutpreferences['quantity'],
                    'unit_price': int(d_payment_preferences.price),
                    'currency_id': d_payment_preferences.currency
                }
            ],
            'payment_methods':{
                'excluded_payment_methods': mp_checkoutpreferences['excluded_payment_methods'],
                'excluded_payment_types': mp_checkoutpreferences['excluded_payment_types']
            },
            'back_urls': {
                'success': 'https://' + request.get_host() + reverse('payments:mp_payment_success', kwargs={'reservation_id': reservation_id})
            },
            'marketplace_fee': mp_checkoutpreferences['marketplace_fee'],
            'external_reference': reservation.pk,
            'notification_url': 'https://' + request.get_host() + reverse('payments:mp_notification')
        }
        
        url_pay = __mp_make_payment_url(mp, preference)

        if(url_pay):
            return redirect(url_pay)
        else:
            if account:
                if __mp_update_token(account):
                    mp = mercadopago.MP(account.access_token)
                    url_pay = __mp_make_payment_url(mp, preference)
                    return redirect(url_pay)
                else:
                    params = {
                        'primary_text': u'Se ha producido un error',
                        'secondary_text': u'ContÃ¡ctenos para revisar que ha podido pasar.',
                        'icon_text':'alert'
                    }
                    return render(request, 'mp_auth_success.html', params)
    return HttpResponse(status=200)


def __mp_make_payment_url(mp, preference):
    try:
        preferenceResult = mp.create_preference(preference)
        url_pay = str(preferenceResult['response']['init_point'])
        return url_pay
    except KeyError as e:
        print(e)
        return False


def __mp_update_token(account):
    url = 'https://api.mercadopago.com/oauth/token'

    json_data = {
        'client_id': MP_APP_ID,
        'client_secret': MP_APP_SECRET,
        'grant_type': 'refresh_token',
        'refresh_token': account.refresh_token
    }

    params = {'jsonRequest': json.dumps(json_data)}

    r = requests.post(url, data=json_data)
    response = r.json()

    if response:
        try:
            account.access_token = response['access_token']
            account.public_key = response['public_key']
            account.refresh_token = response['refresh_token']

            account.save()
            return True
        except KeyError as e:
            print(e)
            pass
    return False

def mp_payment_success(request, reservation_id):
    reservation = get_object_or_404(Reservation, pk=reservation_id)
    doctor = reservation.doctor
    doctor_office = doctor.office.first()
    d_payment_preferences = DoctorPaymentPreferences.objects.get(doctor=doctor)
    block_number = ScheduleBlock.objects.get(pk=reservation.block.pk)

    reservation_detail = {
        'date': date_to_string_format(reservation.appointment_date),
        'block': block_number.display_block(),
        'amount': d_payment_preferences,
        'place': doctor_office
    }

    params = {
        'doctor': doctor,
        'reservation': reservation,
        'reservation_detail':reservation_detail,
        'url_back': 'https://' + request.get_host() + reverse('directory:profile', kwargs={'display_name': doctor.profile.display_name})
    }

    return render(request, 'payment_success.html', params)

def mp_process_payment(request, payment_id):
    mp = mercadopago.MP(MP_APP_ID, MP_APP_SECRET)
    mp_payment = mp.get_payment(payment_id)
    response = mp_payment['response']

    if response['collection']['status']=='approved':
        reservation_id = response['collection'].get('external_reference')

        if reservation_id:
            reservation = get_object_or_404(Reservation, pk=reservation_id)
            reservation.status = 'Confirmada'
            reservation.payment_id = payment_id
            reservation.confirmed_at = datetime.now()
            reservation.save()

            reservation_block = ScheduleBlock.objects.get(pk=reservation.block.pk)

            reservation_detail = dict()
            reservation_detail['date'] = date_to_string_format(reservation.appointment_date)
            reservation_detail['block'] = reservation_block.display_block()

            notification_mail(request, reservation, reservation_detail)
            notification_sms(reservation, reservation_detail)

@csrf_exempt
def mp_notification(request, **kwargs):
    
    log_info(request, request.GET)

    if request.method == 'POST':
        if request.GET.get('topic') == 'payment':
           mp_process_payment(request, request.GET.get('id'))
           return HttpResponse(status=200)

    return HttpResponse(status=200)


def notification_mail(request, reservation, reservation_detail):
    doctor = reservation.doctor
    doctor_office = doctor.office.first()
    host_url = 'https://' + request.get_host()
    sender_email = 'noreply@%s.com' % (request.get_host())

    try:
        # -- Mail to user --
        c_mail_content = dict(MAIL_RESERVATION_TEMPLATES['payment_success_client'])
        c_mail_content['denomination'] = doctor.denomination
        c_mail_content['profile_name'] = doctor.display_fullname()
        c_mail_content['greeting'] = c_mail_content['greeting'] % \
        {'client_name': reservation.client_name}
        c_mail_content['detail_date'] = reservation_detail['date']
        c_mail_content['detail_block'] = reservation_detail['block']
        c_mail_content['server_url'] = host_url
        c_mail_content['sender'] = doctor.display_fullname()
        c_mail_content['detail_place'] = doctor_office.attention_center.name
        c_mail_content['detail_address'] = '%s - %s' % (doctor_office.attention_center.address, doctor_office.description)
        c_mail_content['doctor_contact'] = doctor.display_contacts()

        send_mail(sender_email, reservation.email, c_mail_content['title_mail'], c_mail_content, 'mail/email_notification_client.html')

        # -- Mail to doctor --
        d_mail_content = dict(MAIL_RESERVATION_TEMPLATES['payment_success_doctor'])
        d_mail_content['denomination'] = doctor.denomination
        d_mail_content['profile_name'] = doctor.display_fullname()
        d_mail_content['body'] = d_mail_content['body'] % \
        {'detail_date': reservation_detail['date'], 'detail_block': reservation_detail['block']}
        d_mail_content['server_url'] = host_url
        d_mail_content['detail_name'] = d_mail_content['detail_name'] % \
        {'client_name': reservation.client_name}
        d_mail_content['detail_identity'] = d_mail_content['detail_identity'] % \
        {'client_identity': reservation.display_clientid()}
        d_mail_content['detail_phone'] = d_mail_content['detail_phone'] % \
        {'client_phone': reservation.phone}
        d_mail_content['detail_email'] = d_mail_content['detail_email'] % \
        {'client_email': reservation.email}
        d_mail_content['sender'] = u'Medic'

        send_mail(sender_email, doctor.account.email, d_mail_content['title_mail'], d_mail_content, 'mail/email_notification_doctor.html')        
        
    except Exception as e:
        raise e

def notification_sms(reservation, reservation_detail):
    textveloper_api = TextveloperImproved(SMS_TOKEN_ACCOUNT, SMS_TOKEN_APP)

    doctor = reservation.doctor
    doctor_phone = doctor.account.phone
    client_phone = reservation.phone

    try:  
        # -- SMS to user --
        c_sms_content = dict(SMS_TEMPLATES['payment_success_client'])
        c_sms_content['sms_content'] = c_sms_content['sms_content'] % \
        {'detail_date': reservation_detail['date'], 'detail_block': reservation_detail['block'], 'denomination': doctor.denomination, 'doctor_name': doctor }
        
        textveloper_api.send_sms(client_phone, c_sms_content['sms_content'])

        # -- SMS to doctor --
        d_sms_content = dict(SMS_TEMPLATES['payment_success_doctor'])
        d_sms_content['sms_content'] = d_sms_content['sms_content'] % \
        {'detail_date': reservation_detail['date'], 'detail_block': reservation_detail['block'], 'client_name': reservation.client_name, 'client_identity':reservation.display_clientid(), 'client_phone': reservation.phone, 'client_email':reservation.email}

        textveloper_api.send_sms(doctor_phone, d_sms_content['sms_content'])

    except Exception as ex:
        print(ex)