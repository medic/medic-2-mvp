from django.contrib import admin
from .models import MercadopagoCredentials, DoctorPaymentPreferences

# Register your models here.

@admin.register(MercadopagoCredentials)
class MercadopagoCredentialsAdmin(admin.ModelAdmin):
    list_display = ('access_token', 'public_key', 'doctor')

@admin.register(DoctorPaymentPreferences)
class DoctorPaymentPreferencesAdmin(admin.ModelAdmin):
    list_display = ('doctor', 'mp_method', 'price', 'currency')