from django.db import models

from directory.models import Doctor

# Create your models here.

class DoctorPaymentPreferences(models.Model):
    doctor = models.OneToOneField(Doctor, on_delete=models.CASCADE, primary_key=True, related_name='payment_preferences')
    mp_method = models.CharField('método de pago', max_length=20, default='mp_standard', choices=(('mp_standard', 'Mercado Pago Standard'), ('mp_marketplace', 'Mercado Pago Marketplace')))

    price = models.DecimalField('precio', max_digits=30, decimal_places=2)
    currency = models.CharField('moneda', max_length=5, choices=(('VEF', 'Bs'),))

    def __str__(self):
        return '%s %s' % (self.price, self.get_currency_display())
    
    class Meta:
        verbose_name = 'Preferencia de consulta'
        verbose_name_plural = 'Preferencias de consulta'


class MercadopagoCredentials(models.Model):
    access_token = models.TextField(max_length=100)
    public_key = models.TextField(max_length=50)
    refresh_token = models.TextField(max_length=100)
    doctor = models.OneToOneField(Doctor, on_delete=models.CASCADE, primary_key=True)