$( document ).ready(function() {
    paintMap()

    eventHeader()

    goThisWeek();

    $('#go-registry').click(function(e){
        e.preventDefault();
        $('html, body').animate({scrollTop:$('#schedule').offset().top - 20}, 1500)
    })

    $('#next-week').click(function(e){
        moveWeek(1);
    });

    $('#last-week').click(function(e){
        moveWeek(-1);
    });

    validateReservationForm()
});

function validateReservationForm(){
    var phone = document.getElementById("phone")
    
    phone.addEventListener("keyup", function (event) {
      if (phone.validity.patternMismatch) {
        phone.setCustomValidity("Ingresa un teléfono con formato 0000-000000")
      } else {
        phone.setCustomValidity("")
      }
    })
}

function paintMap(){
    var myLatlng = new google.maps.LatLng(PLACE_LAT, PLACE_LNG);
	var myOptions = {
		zoom: 17,
		center: myLatlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		scrollwheel: false
	}
	var map = new google.maps.Map(document.getElementById("consultation-map"), myOptions);

    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: PLACE_NAME
    })
}

function eventHeader(){
    $(window).scroll(function () {
        var div_height = $(".group-section").height()
        if ($(this).scrollTop() > div_height - (div_height * (10 / 100)))
            $('#header').show()
        else
            $('#header').hide()
    })
}

function selectBlock(element){
    if (! $('#form-reservation').is(':visible')){
        $('#form-reservation').fadeIn('slow');
        $('html, body').animate({scrollTop:$('#form-reservation').offset().top - 85}, 1500)
    }

    updateStatus(element);
    updateReservationDetail($('#'+element+'-value-start').text(), $('#'+element+'-value-end').text() , $('#'+element.substring(0, element.indexOf("-"))+'-day-date').text(), $('#'+element+'-block-number').text(), $('#'+element+'-block-limit').text());
}

function updateReservationDetail(block_time_start, block_time_end, reservation_date, block_number, block_limit){
    $('#text-block-time').text(block_time_start + ' - ' + block_time_end);
    $('#block-time-start').val(block_time_start);      
    $('#block-time-end').val(block_time_end);          

    $('#text-reservation-date').text(reservation_date);
    $('#reservation-date').val(reservation_date);

    $('#block-number').val(block_number);
    $('#block-limit').val(block_limit);
}