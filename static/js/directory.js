$( document ).ready(function() {
    $('.directory-filter').change(function(){
        filter_directory()
    })
})

function filter_directory(){
    var specialty_filter, city_filter, result

    result = false
    $('.item-card').hide()

    if ($('.result-message').is(":visible"))
        $('.result-message').hide()

    $('.item-card').each(function(){
        specialty_filter = filterby($(this).find(".specialty-filter"), $('#specialty').val())
        city_filter = filterby($(this).find(".city-filter"), $('#city').val())
        
        if (specialty_filter && city_filter){
            result = true
            $(this).delay(100).fadeIn()
        }            
    })

    if (!result)
        $('.result-message').show()
}

function filterby(element, filter_val){
    var isvalid = false
    filter_val = filter_val.toLowerCase()

    element.each(function(){
        if (($(this).val()).toLowerCase() == filter_val || filter_val == 'all')
            isvalid = true
    })

    return isvalid
}