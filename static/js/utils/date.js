function toDate(dateStr) {
    var parts = dateStr.split("/");
    return new Date(parts[2], parts[1] - 1, parts[0]);
}

function toDateFormat(d_date){
    return toDayFormat((d_date.getDate()).toString()) + '/' + toDayFormat((d_date.getMonth() + 1).toString()) + '/' + d_date.getFullYear();
}

function toDayFormat(s_number){
    return s_number.length < 2 ? '0' + s_number : s_number;
}

function startWeek(d_date){
    var day = d_date.getDay() || 7;
    if (day !== 1)
        d_date.setHours(-24 * (day - 1));
    return d_date;
}

function initializeDate(d_date, advance, days, move){
    if (move == 0)
        move = 1 
    if (advance >= 0)
        d_date.setDate(d_date.getDate() + (days * move))        
    else
        d_date.setDate(d_date.getDate() - (days * move))
    d_date.setHours(0,0,0,0)
    return d_date
}

function toDatetime(d_date, s_time12){
    return new Date((d_date.getMonth() + 1) +'/' + d_date.getDate() +'/' + d_date.getFullYear()+ ' ' + s_time12)
}

function moveFromDate(days, d_date){
    time_date = d_date.getTime();
    milliseconds = parseInt(days * 24 * 60 * 60 * 1000);
    d_date.setTime(time_date + milliseconds);
    return d_date
}