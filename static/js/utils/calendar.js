//Building
"use strict";
var max_advance = 1,
    max_week = startWeek(new Date()),
    this_week = startWeek(new Date()),
    today = new Date()


today.setHours(0,0,0,0)

function getCalendar(on_load, start_date){
    var today_date = toDateFormat(new Date())
    var display_name = $('#display_name').text()

    $.post('/reservation/api/calendar/' + display_name + '/', {on_load: on_load, start_date: start_date, today_date: today_date}, function(data){
        
        var times_block = data.working_days;
        max_advance = data.max_advance;
        var start_week = data.start_week

        max_week = initializeDate(startWeek(new Date()), 1, 7, max_advance)
        
        scheduleData(start_week, times_block);
        initializeBlocksButtons();
    }).done(function(){
        loaderAction(false)
    });
}

function scheduleData(start_date, data){
    var item='', 
        container, 
        max_blocks = longestJourney(data), 
        blocks_day,
        day_date,
        block_status = true,
        start_week = toDate(start_date)

    var last_date = new Date(start_week.getTime());    
    moveFromDate(-1, last_date)

    var s_first_day = toDateFormat(start_week),
        s_last_day;

    $('#week_date').html(toDateFormat(start_week));

    $('#month-name').html(changeMonth(start_week))

    for (var i = 0; i < 5; i++){        
        last_date.setDate(last_date.getDate() + 1);

        s_last_day = toDateFormat(last_date);
        createCalendarDay(i, last_date.getDate(), s_last_day);
    }

    $('.days').find('[class*=reservation-block]').remove()

    for (var day in data){
        container  = $('#'+day).find('.content');
    
        day_date = toDate($('#' + day + '-day-date').html())

        blocks_day = data[day].length;

        for (var i = 0; i < max_blocks; i++){
            if (i < blocks_day){                
                if (day_date.getTime() < today.getTime())
                    block_status = false
                else if ((day_date.getTime() == today.getTime()) && toDatetime(today, data[day][i].end).getTime() < (new Date()).getTime())
                    block_status = false
                else
                    block_status = data[day][i].available

                item = createReservationBlock(day+'-'+i, block_status, data[day][i].start, data[day][i].end, data[day][i].id, data[day][i].limit)                
            }else
                item = createReservationBlock(day+'-'+i, true,'', '', 0)
            container.append(item);
        }       
    }
    
    var week_date = toDate($('#week_date').html())        
        week_date.setHours(0,0,0,0)

    if  (max_advance <= 0 || week_date.getTime() >= max_week.getTime())
        $('#next-week').attr('disabled', true)

    if (week_date.getTime() > this_week.getTime())
        $('#last-week').removeAttr('disabled')
}

//Utils

function longestJourney(week_blocks){
    if (!week_blocks || week_blocks == null)
        return 0;
    var max_blocks = 0;
    var total_blocks = 0;

    for (var day in week_blocks){
        total_blocks = week_blocks[day].length;
        if (max_blocks < total_blocks)
            max_blocks = total_blocks;
    }
    return max_blocks;    
}

//Interactivity

function changeMonth(d_date){
    var options = {
        month: 'long'
    }
    return d_date.toLocaleDateString('es-MX', options)
}

function goWeek(on_load, start_week){
    var s_first_day = toDateFormat(start_week);

    getCalendar(on_load, s_first_day);
}

function goThisWeek(){
    var week_date = startWeek(new Date())
    goWeek(1, week_date)
}

function moveWeek(s_move){
    if (s_move && ( s_move > 0) || s_move < 0){

        loaderAction(true)

        var week_date = toDate($('#week_date').html())
        week_date.setHours(0,0,0,0)
        this_week.setHours(0,0,0,0)
        
        if (s_move > 0) {            
            week_date.setTime(moveFromDate(7, week_date))

            if (week_date.getTime() > this_week.getTime())
                $('#last-week').removeAttr('disabled')
            if (week_date.getTime() >= max_week.getTime())
                $('#next-week').attr('disabled', true)
        }else if(s_move < 0){            
            week_date.setTime(moveFromDate(-7, week_date))

            var next_button = $('#next-week')
            if (next_button.attr('disabled'))
                next_button.removeAttr('disabled')
                
            if (week_date.getTime() <= this_week.getTime())
                $('#last-week').attr('disabled', true)
        }
        goWeek(0, week_date)
    }
}

function updateStatus(element){
    $('.reservation-block .icon-calendar-check').each(function(index){
        $(this).removeClass('icon-calendar-check');
        $(this).addClass('icon-calendar');
    });

    $('.reservation-block .selected').first().each(function(index){
        $(this).removeClass('selected');
    })

    $('#'+ element + ' .icon-calendar').each(function(index){
        $(this).removeClass('icon-calendar');
        $(this).addClass('icon-calendar-check');
        $('#'+ element).addClass('selected');
    });
}

function initializeBlocksButtons(){
    $('button[name = "reservable"]').click(function(e){
        selectBlock($(this).attr("id"));
    });
}

//Content

function createReservationBlock(id_element, status_block, value_start, value_end, block_number, block_limit){
    var html = '<div class="reservation-block-empty"></div>';
    var element_name = status_block ? 'reservable' : 'non-reservable';

    if (value_start != '' && value_end != ''){  
        html = '<div class="reservation-block normal"> <button id="'+id_element+'" name="'+ element_name +'" class="'+element_name+' btn btn-default '+ (element_name != 'reservable' ? 'inactive" disabled = "true"' : '"') +' data-toggle="tooltip" data-placement="top" title="Este bloque '+ (status_block ? '' : 'no ') +' se encuentra disponible" > <div class="reservation-icon"> <i class="icon icon-calendar'+ (status_block ? '' : '-remove') +'" aria-hidden="true"></i> </div> <div class="reservation-text"> <div> <div id="'+id_element+'-value-start" class="hidden">'+value_start+'</div> <div class="row hidden-xs">'+value_start+'</div> <div class="row hidden-xs">a</div> <div id="'+id_element+'-value-end" class="hidden">'+value_end+'</div> <div class="row hidden-xs">'+value_end+'</div> <div class="visible-xs-inline"> '+ value_start +' - ' +value_end+'</div> </div> </div> <div id="'+id_element+'-block-number" style="display:none">'+ block_number +'</div><div id="'+id_element+'-block-limit" style="display:none">'+ block_limit +'</div></button> </div>';
    }    
    return html;
}

function createCalendarDay(n_day, d_day, s_date){
    var week_days = ['monday', 'tuesday','wednesday', 'thursday', 'friday'];
    var element = $('#' + week_days[n_day]).find('.day-date');  
    
    element.children().remove();
    element.append('<span class="day-number">' + d_day + '</span><div id="' + week_days[n_day] + '-day-date">' + s_date + '</div>');
}

function loaderAction(active){    
    if (!active){
        $('#schedule-loader').hide()
        $('#schedule-days').show()
    }else{
        $('#schedule-days').hide()
        $('#schedule-loader').show()
    }
}