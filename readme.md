
## Instalar dependencias

Instalar Pipenv

```bash
sudo pip install pipenv
```

Crear entorno virtual e instalar dependencias.

```bash
pipenv install
```

Activar entorno virtual

```bash
pipenv shell
```

## Correr proyecto en local con Heroku

Instalar heroku CLI https://devcenter.heroku.com/articles/heroku-cli

Loguear con cuenta de Heroku

```bash
heroku login
```

Crear archivo .env y usar archivo env-sample de referencia para colocar configuraciones del entorno.

Correr proyecto

```bash
heroku local web -f Procfile-dev
```

## Otros comandos

Correr comando de Django con entorno local de Heroku

```bash
heroku local:run ./manage.py <comando>
```

Agregar dependencia a entorno y Pipfile

```bash
pipenv install <dependencia>
```

Agregar un nuevo remote a tu repositorio llamado heroku

```bash
heroku git:remote -a medic-testing
```

Deployment

```bash
git push heroku master
```

Correr comando de Django en instancia de Heroku

```bash
heroku run ./manage.py <comando>
```

## Datos de prueba para mercadopago

Marketplace: TETE8203377 / qatest8084
Doctor: TT807749 / test_user_60316971@testuser.com / qatest6546
Paciente: TETE6714751 / test_user_95270356@testuser.com / qatest3707

Tarjeta de prueba: 	4966 3823 3110 9310

Otras tarjetas e instrucciones
https://www.mercadopago.com.ve/developers/en/solutions/payments/basic-checkout/test/test-payments/



