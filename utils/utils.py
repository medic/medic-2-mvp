#from data.reservation_data import CHECKOUT_PREFERENCES
from datetime import datetime, date

def to_vef_format(num):
    return ('{:,}'.format(num).replace(',','.'))

def to_12h_format(string24h):
   time24h = datetime.strptime(string24h, "%H:%M")
   return time24h.strftime("%I:%M %p")

def to_24h_format(string12h):
    time12h = datetime.strptime(string12h, "%I:%M %p")
    return time12h.strftime("%H:%M")

def to_24h_object(string12h):
    time12h = datetime.strptime(string12h, "%I:%M %p")        
    return time12h.time()

def to_date_format(s_date):
    return datetime.strptime(s_date, "%d/%m/%Y").date()

def date_to_string_format(d_date):
    return d_date.strftime('%d/%m/%Y')

def to_phone_format(s_phone):
    return s_phone.replace('-','')
