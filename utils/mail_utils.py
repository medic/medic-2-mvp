#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sendgrid
from sendgrid.helpers.mail import *
from django.template.loader import render_to_string

def send_mail(mail_from, mail_to, subject, mail_content, template):
    
    sg = sendgrid.SendGridAPIClient(apikey=os.environ.get('SENDGRID_API_KEY'))
    from_email = Email(mail_from)
    to_email = Email(mail_to)

    template_render = render_to_string(template, mail_content)
    content = Content('text/html', template_render)
    mail = Mail(from_email, subject, to_email, content)
    response = sg.client.mail.send.post(request_body=mail.get())

    return response.status_code