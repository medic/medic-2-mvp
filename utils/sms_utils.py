import json
import re
import requests

class Textveloper:
        
    def __init__(self, cuenta_token, aplicacion_token):
        self.cuenta_token = cuenta_token
        self.aplicacion_token = aplicacion_token

    def enviar_sms(self,telefono, mensaje):
        try:

            url = "https://api.textveloper.com/sms/enviar/"

            # Web Hook Data Parameters
            json_data = {
                'telefono': str(telefono),
                'mensaje': str(mensaje),
                'aplicacion_token': str(self.aplicacion_token),
                'cuenta_token': str(self.cuenta_token)
            }
                
            params = {'jsonRequest': json.dumps(json_data)}

            r = requests.post(url, data=json_data)
            response = r.json()
            
            if r.status_code == 200 and response['respuesta'] == 'ok':
                print("Mensaje Enviado al telefono: " + telefono)
            else:
                print("Mensaje No enviado, Error: " + response['detalle'])

        except Exception as ex:
            print(ex)

class TextveloperImproved(Textveloper): 
    def __init__(self, token_account, token_application):        
        Textveloper.__init__(self, token_account, token_application)
    
    def validate_sms(self, content):
        return content.split('++ ') if len(content) > 140 else [content.replace('++ ', '')]

    def send_sms(self, phone, content):
        if len(content):
            content.encode('utf-8')
            sms_parts = self.validate_sms(content)
            parts = len(sms_parts)

            for part in range(0, parts):
                self.enviar_sms(phone, sms_parts[part])