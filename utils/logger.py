from logentries import LogentriesHandler
import logging
import os


try:
    logentries_token = os.environ['LOGENTRIES_TOKEN']
    log = logging.getLogger('logentries')
    log.setLevel(logging.INFO)
    log.addHandler(LogentriesHandler('96347ed5-5e76-4baf-8e3b-6e2de0cdd539'))
except KeyError:
    log = logging.getLogger('')
    log.setLevel(logging.INFO)

def log_info(request, content):
    log.info("%s - %s" % (request.path, content) )

def log_warn(request, content):
    log.warn("%s - %s" % (request.path, content) )


def log_error(request, content):
    log.error("%s - %s" % (request.path, content) )


def log_critical(request, content):
    log.critical("%s - %s" % (request.path, content) )