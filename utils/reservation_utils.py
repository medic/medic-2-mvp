#!/usr/bin/env python
# -*- coding: utf-8 -*-

from directory.models import Doctor
from django.forms.models import model_to_dict
from utils.utils import to_12h_format

from datetime import datetime, date, timedelta
from utils.utils import to_12h_format, date_to_string_format, to_date_format

import calendar

def get_week(on_load, display_name, start_date, today_date):
    
    doctor = Doctor.objects.filter(profile__display_name=display_name).get()
    profile = Doctor.profile

    times_block = doctor.schedule_block.order_by('day', 'start')
    
    week = False
    i = 0
    max_advance = 1 #TODO: colocar aqui desde model

    max_week = start_date + timedelta(days=7*max_advance)

    while (week == False) and (i <= max_advance):
        week_date = start_date + timedelta(days=7 * i)

        week = get_status_week(on_load, times_block, max_week, week_date, today_date)

        i += 1
    
    calendar_week = {
        'times_block': week,
        'start_week': date_to_string_format(week_date)
    }

    return calendar_week

def get_status_week(on_load, times_block, max_week, start_date, today_date):
    week_blocks = times_block
    isavailable = False

    dict_blocks = create_week()

    for i in range(0, 5):
        date_day = start_date + timedelta(days=i)

        day_name = date_day.strftime('%A').lower()
        blocks_day = week_blocks.filter(day=day_name)
        day_trip = blocks_day.count()

        for block in blocks_day.all():
            block_day = model_to_dict(block)
            block_day['start'] = to_12h_format(block_day['start'].strftime('%H:%M'))
            block_day['end'] = to_12h_format(block_day['end'].strftime('%H:%M'))           
            
            dict_blocks[day_name].append(block_day)

    return dict_blocks

def create_week():
    week_days = dict()
    for j in range(0,5):
        week_days[(calendar.day_name[j]).lower()] = []    
    return week_days