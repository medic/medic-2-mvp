#!/usr/bin/env python
# -*- coding: utf-8 -*-

MP_CHECKOUT_PREFERENCES = {
    'title': 'Consulta',
    'description': 'Reservación de consulta',
    'quantity': 1,
    'marketplace_fee': 10,
    'excluded_payment_methods':[
        {'id': 'amex'},
        {'id': 'naranja'},
        {'id': 'nativa'},
        {'id': 'tarshop'},
        {'id': 'cencosud'},
        {'id': 'cabal'},
        {'id': 'diners'},
        {'id': 'pagofacil'},
        {'id': 'argencard'},
        {'id': 'maestro'},
        {'id': 'devmaster'},
        {'id': 'debcabal'},
        {'id': 'debvisa'},
        {'id': 'rapipago'},
        {'id': 'redlink'},
        {'id': 'bapropagos'},
        {'id': 'cordial'},
        {'id': 'cordobesa'},
        {'id': 'cmr'},
        {'id': 'consumer_credits'}
    ],
    'excluded_payment_types': [
        {'id': 'ticket'},
        {'id': 'debit_card'},
        {'id': 'atm'},
        {'id': 'bank_transfer'},
        {'id': 'prepaid_card'},
        {'id': 'digital_currency'}
    ]
}
