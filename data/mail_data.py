#!/usr/bin/env python
# -*- coding: utf-8 -*-

MAIL_RESERVATION_TEMPLATES = {
    'payment_success_client': {
        'title_mail': u'Confirmación de reserva',
        'denomination': u'%(denomination)s',
        'profile_name': u'%(doctor_name)s',
        'greeting': u'Hola %(client_name)s',
        'body': u'Le informamos que su reserva ha sido confirmada.',
        'sender': u'%(sender)s',
        'detail_title': u'Detalle de cita',
        'detail_date': u'%(detail_date)s',
        'detail_block': u'%(detail_block)s',
        'detail_place': u'%(doctor_place_name)s',
        'detail_address': u'%(doctor_place_address)s',
        'server_url': '%(server)s'
    },
    'payment_success_doctor': {
        'title_mail': u'Reserva confirmada',
        'denomination': u'%(denomination)s',
        'profile_name': u'%(doctor_name)s',
        'greeting': u'Hola',
        'body': u'Le informamos que tiene una nueva reserva para la fecha %(detail_date)s en el bloque de %(detail_block)s.',
        'sender': u'%(sender)s',
        'detail_title': u'Datos de reserva',
        'detail_name': u'%(client_name)s',
        'detail_identity': u'%(client_identity)s',
        'detail_phone': u'%(client_phone)s',
        'detail_email': u'%(client_email)s',
        'server_url': '%(server)s'
    }
}