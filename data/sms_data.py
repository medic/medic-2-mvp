#!/usr/bin/env python
# -*- coding: utf-8 -*-
SMS_TEMPLATES = {
    'payment_success_client':{
        'sms_content': u'Reserva confirmada para el %(detail_date)s de %(detail_block)s. ++ Atte: %(doctor_name)s'
    },
    'payment_success_doctor':{
        'sms_content': u'Reserva para el %(detail_date)s de %(detail_block)s a nombre de %(client_name)s %(client_identity)s. ++ Contacto: %(client_phone)s - %(client_email)s'
    }
}
